/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isspace.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/04 15:06:01 by algrele           #+#    #+#             */
/*   Updated: 2018/04/04 15:08:32 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

/*
** Like the C++ funct, checks if char c is a whitespace character.
** ' ' space, '\t' horizontal tab, '\v' vertical tab, '\n' newline
** '\f' feed, '\r' carriage return
*/

int		ft_isspace(char c)
{
	if (c == ' ' || c == '\n' || c == '\t' || c == '\v' \
			|| c == '\r' || c == '\f')
		return (TRUE);
	return (FALSE);
}
