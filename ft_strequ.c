/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strequ.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/04 19:31:06 by algrele           #+#    #+#             */
/*   Updated: 2018/04/04 19:52:56 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

int	ft_strequ(char const *s1, char const *s2)
{
	size_t	i;

	i = 0;
	if (!s1 || !s2)
		return (FALSE);
	while (s1[i] && s2[i] && s1[i] == s2[i])
		i++;
	if (i != ft_strlen(s1) || i != ft_strlen(s2))
		return (FALSE);
	else
		return (TRUE);
}
