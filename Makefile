# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: algrele <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/04/03 22:30:52 by algrele           #+#    #+#              #
#    Updated: 2018/04/10 14:54:26 by algrele          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = libft.a

SRC = ft_isalpha.c \
	ft_isdigit.c \
	c_in_macro.c \
	ft_isalnum.c \
	ft_isascii.c \
	ft_isprint.c \
	ft_isspace.c \
	ft_what_alnum.c \
	ft_putchar.c \
	ft_putchar_fd.c \
	ft_putstr.c \
	ft_putstr_fd.c \
	ft_putnbr.c \
	ft_putnbr_fd.c \
	ft_putendl.c \
	ft_putendl_fd.c \
	ft_putsign.c \
	ft_putsign_fd.c \
	ft_putnbrl.c \
	ft_putnbrl_fd.c \
	ft_putnbr_base.c \
	ft_putstr_non_printable.c \
	ft_putstr_replace.c \
	ft_putlst.c \
	ft_putchar_c.c \
	ft_putstr_c.c \
	ft_putnbr_c.c \
	ft_putendl_c.c \
	ft_putnbrl_c.c \
	ft_strlen.c \
	ft_strcpy.c \
	ft_strncpy.c \
	ft_strstr.c \
	ft_strcat.c \
	ft_strncat.c \
	ft_strlcat.c \
	ft_strnstr.c \
	ft_strcmp.c \
	ft_strncmp.c \
	ft_strequ.c \
	ft_strnequ.c \
	ft_strchr.c \
	ft_strrchr.c \
	ft_toupper.c \
	ft_tolower.c \
	ft_strrev.c \
	ft_striter.c \
	ft_striteri.c \
	ft_strmap.c \
	ft_strmapi.c \
	ft_foreach.c \
	ft_bzero.c \
	ft_strnew.c \
	ft_strdel.c \
	ft_strclr.c \
	ft_strdup.c \
	ft_strsub.c \
	ft_strjoin.c \
	ft_strtrim.c \
	ft_strctrim.c \
	ft_strsplit.c \
	ft_memalloc.c \
	ft_memdel.c \
	ft_memcpy.c \
	ft_memcmp.c \
	ft_memchr.c \
	ft_memset.c \
	ft_memmove.c \
	ft_memccpy.c \
	ft_swap.c \
	ft_factorial.c \
	ft_sqrt.c \
	ft_range.c \
	ft_power.c \
	ft_atoi.c \
	ft_atoi_base.c \
	ft_itoa.c \
	ft_itoa_base.c \
	ft_convert_base.c \
	ft_lstnew.c \
	ft_lstdelone.c \
	ft_lstdel.c \
	ft_lstadd.c \
	ft_lstiter.c \
	ft_lstmap.c

OBJ = $(SRC:.c=.o)

CFLAGS = -Wall -Wextra -Werror

INCL_DIR = ./

all: $(NAME)

$(NAME): $(OBJ)
	@ar -rc $(NAME) $(OBJ)
	@ranlib $(NAME)

%.o : %.c
	@gcc $(CFLAGS) -I $(INCL_DIR) -o $@ -c $<

clean:
	@rm -f $(OBJ)

fclean : clean
	@rm -f $(NAME)

re : fclean all

.PHONY: all clean fclean re
