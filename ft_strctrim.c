/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strctrim.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/06 16:43:12 by algrele           #+#    #+#             */
/*   Updated: 2018/04/06 16:49:58 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

/*
** allocate and return a copy of s string while removing all c chars at the
** start and end of string.
*/

char	*ft_strctrim(char const *s, char c)
{
	char	*trim;
	size_t	s1;
	size_t	s2;

	if (!s)
		return (NULL);
	if (!c)
		return (ft_strdup(s));
	s1 = 0;
	while (s[s1] == c)
		s1++;
	if (s1 != ft_strlen(s))
	{
		s2 = 1;
		while (s[ft_strlen(s) - s2] == c)
			s2++;
		s2--;
		if (ft_strlen(s) > 0)
		{
			if (!(trim = ft_strnew(ft_strlen(s) - s1 - s2)))
				return (NULL);
			return (ft_strncpy(trim, s + s1, ft_strlen(s) - s1 - s2));
		}
	}
	return (ft_strnew(1));
}
