/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_what_alnum.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/03 22:45:25 by algrele           #+#    #+#             */
/*   Updated: 2018/04/04 14:39:20 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

/*
** Returns 0 if lowercase, 1 if uppercase, 2 if number, -1 otherwise
*/

int	ft_what_alnum(int c)
{
	if (c >= 'a' && c <= 'z')
		return (LOWCASE);
	if (c >= 'A' && c <= 'Z')
		return (UPCASE);
	if (c >= '0' && c <= '9')
		return (NUMCASE);
	return (-1);
}
