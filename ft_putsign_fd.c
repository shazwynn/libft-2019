/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putsign_fd.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/04 14:46:19 by algrele           #+#    #+#             */
/*   Updated: 2018/04/04 14:50:46 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

/*
** PUTSTR_FD "ZERO", "NEGATIVE", "POSITIVE" depending on number sign
*/

void	ft_putsign_fd(int n, int fd)
{
	if (n > 0)
		ft_putstr_fd("positive", fd);
	if (n < 0)
		ft_putstr_fd("negative", fd);
	if (n == 0)
		ft_putstr_fd("zero", fd);
}
