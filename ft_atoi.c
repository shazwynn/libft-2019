/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/06 17:10:29 by algrele           #+#    #+#             */
/*   Updated: 2018/04/06 18:03:19 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

int		ft_atoi(const char *str)
{
	int	res;
	int	i;
	int	s;

	res = 0;
	i = 0;
	s = 0;
	if (str[s] == '\200')
		return (0);
	while (ft_isspace(str[s]))
		s++;
	if (str[s] == '+' || str[s] == '-')
		i++;
	while (ft_isdigit(str[i + s]))
	{
		res = res * 10 + str[i + s] - '0';
		i++;
	}
	if (str[s] == '-')
		res = -res;
	return (res);
}
