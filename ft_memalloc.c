/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memalloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/04 22:13:05 by algrele           #+#    #+#             */
/*   Updated: 2018/04/04 22:18:46 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

/*
** allocates memory zone of indicated size and initializes memory to 0
*/

void	*ft_memalloc(size_t size)
{
	void	*new;

	if (!(new = (void *)malloc(size)))
		return (NULL);
	ft_bzero(new, size);
	return (new);
}
