/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putlst.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/07 00:06:08 by algrele           #+#    #+#             */
/*   Updated: 2018/04/07 00:06:20 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

void	ft_putlst(t_list *list)
{
	while (list)
	{
		ft_putstr("[ ");
		ft_putstr((char *)list->content);
		ft_putstr(" ] -> ");
		list = list->next;
	}
	ft_putstr("NULL\n");
}
