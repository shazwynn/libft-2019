/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/06 15:32:57 by algrele           #+#    #+#             */
/*   Updated: 2018/04/06 16:50:31 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

/*
** allocate and return a copy of s string without spaces at the start and end.
** spaces : ' ', '\n', '\t'.
*/

char	*ft_strtrim(char const *s)
{
	char	*trim;
	size_t	s1;
	size_t	s2;
	size_t	len;

	if (!s)
		return (NULL);
	len = ft_strlen(s);
	s1 = 0;
	while (s[s1] == ' ' || s[s1] == '\n' || s[s1] == '\t')
		s1++;
	if (s1 != len)
	{
		s2 = 1;
		while (s[len - s2] == ' ' || s[len - s2] == '\n' || s[len - s2] == '\t')
			s2++;
		s2--;
		if (len > 0)
		{
			if (!(trim = ft_strnew(len - s1 - s2)))
				return (NULL);
			return (ft_strncpy(trim, s + s1, len - s1 - s2));
		}
	}
	return (ft_strnew(1));
}
